# TestGetRead

#### 项目介绍
Android可自定义的矿石收取界面View
===============================
* 实现矿石的随机且不重叠生成（自定义时可能失效）
-------------------------------------------
* 实现矿石的拖拽及越线触发回调（可自定义）
---------------------------------------
* 实现矿石的动画效果上下左右平移及缩放（可自定义）
---------------------------------------------
* 支持各个矿石下显示不同文字
--------------------------
* 大量的可自定义参数（矿石图片，数量，单个矿石的文字，显示动画，展示线，触发线，碰撞体大小等）
-------------------------------------------------------------------------------------

![部分效果展示](https://gitee.com/belong_z/TestGetRead/raw/master/app/src/main/res/drawable/show2.gif "部分效果展示")

#### 软件架构
软件架构说明 

没啥构架，瞎写的
--------------

#### 使用说明
1. 调用顺序，其他方法没顺序，记得最后一定要调用 `.initAll（）`方法
-------------------------------------------------------------
2. 设置回调，实现 `GetRadBoxCallBack` 接口，调用 `.setCall()` 方法即可，如在当前activity中实现回调接口则可省略调用该方法
---------------------------------------------------------------------------------------------------------------
3. 其中可以自定义的方法
----------------------
```Java
        private GetRadBoxView grbv_;
        private void init() {
            //设置回调，如在当前activity中实现回调接口则可省略
            grbv_.setCall(this);
            //文字中心位置与图片中心的偏移比例 0~1
            grbv_.setTextOffset(new PointF(1,1));
            //文字大小
            grbv_.setTextSize(25f);
            //文字颜色值
            grbv_.setTextColor("#ffffff");
            //离散度 0~1
            grbv_.setConcentration(1);
            //单个方块大小比例 0~1
            grbv_.setBoxSize(1f);
            //生成最低线的位置比例 0~1
            grbv_.setShowLine(new PointF(1f,0.7f));
            //触发最高线的位置比例 0~1
            grbv_.setTriggerLine(new PointF(1f,0.8f));
            //触发的碰撞体与box大小比例 0~1
            grbv_.setTriggerBoxLine(new PointF(2f,2f));
            //是否开启动画
            grbv_.setAnimator(true);
            //设置单次动画时间
            grbv_.setAnimatorTime(200);
            //动画的属性,展示效果与box初始大小的比例 左负右正
            //最初x，最终x，最初y，最终y，最初大小，最终大小
            grbv_.setAnimatorData(new float[]{0,0,0.3f,-0.25f,0,0});
            //添加展示数据  imgData：展示的图片  msgDatas：文字内容
            grbv_.setImgAddData(bitmap, strings);
            //初始化，启动必须调用的方法(否则没有效果)
            grbv_.initAll();
        }
```
![相关自定义方法展示](https://images.gitee.com/uploads/images/2018/0710/002713_2ca1e582_2024135.png "屏幕截图.png")


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)