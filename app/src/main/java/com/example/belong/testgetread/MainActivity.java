package com.example.belong.testgetread;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GetRadBoxView.GetRadBoxCallBack{

    private GetRadBoxView grbv_;
    private View btn;
    private Bitmap bitmap;
    private ArrayList<String> strings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        grbv_ = findViewById(R.id.grbv_);
        btn = findViewById(R.id.btn);
        initData();
        init();
        setListener();

    }

    private void initData() {
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.b1);
        strings = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            strings.add(i+"=50 DDC");
        }

    }

    private void setListener() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                grbv_.setImgAddData(bitmap, strings);
                grbv_.initAll();
            }
        });
    }

    private void init() {
        //文字中心位置与图片中心的偏移比例 0~1
        grbv_.setTextOffset(new PointF(0,0f));
        //文字大小
        grbv_.setTextSize(25f);
        //文字颜色值
        grbv_.setTextColor("#ffffff");
        //离散度 0~1
        grbv_.setConcentration(1);
        //单个方块大小比例 0~1
        grbv_.setBoxSize(1f);
        //生成最低线的位置比例 0~1
        grbv_.setShowLine(new PointF(1f,0.7f));
        //触发最高线的位置比例 0~1
        grbv_.setTriggerLine(new PointF(1f,0.8f));
        //触发的碰撞体与box大小比例 0~1
        grbv_.setTriggerBoxLine(new PointF(1f,1f));
        //是否开启动画
        grbv_.setAnimator(true);
        //设置单次动画时间
        grbv_.setAnimatorTime(1000);
        //动画的属性,展示效果与box初始大小的比例 左负右正 最初x，最终x，最初y，最终y，最初大小，最终大小
        grbv_.setAnimatorData(new float[]{0,0.2f,0.3f,-0.25f,0,0.2f});
        //添加展示数据  imgData：展示的图片  msgDatas：文字内容
        grbv_.setImgAddData(bitmap, strings);
        //初始化，启动必须调用的方法(否则没有效果)
        grbv_.initAll();
    }

    @Override
    public void delBox(List<Integer> delIndex, int delThis) {
        Toast.makeText(this,delIndex.toString()+"===="+delThis,Toast.LENGTH_LONG).show();
        Log.e("-------回调---",delIndex.toString()+"===="+delThis);
    }
}
